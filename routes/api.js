var express = require('express');
var router = express.Router();

const mockMessage = (params) => {
  return new Promise((resolve, reject) => {
    // Mocking the delay of the sms transaction
    setTimeout( () => {
      resolve({
        success: true
      });
    }, 500);
  });
};

router.post('/send-message', function(req, res) {
  mockMessage(req.body).then( (response) => {
    res.json(response);
  });
});

module.exports = router;
